package oxoop;


import java.util.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Symon
 */
public class Table {

    private char[][] data = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player currentPlayer, o, x, win;
    private int round;

    Random random = new Random();

    public Table(Player o, Player x) {
        this.o = o;
        this.x = x;
        if (random.nextInt(2) == 0) {
            currentPlayer = o;
        } else {
            currentPlayer = x;
        }
    }

    public char[][] getData() {
        return data;
    }

    public Player getWinner() {
        return win;
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public void addWinLose() {
        if (win == x) {
            x.win();
            o.lose();
        } else if (win == o) {
            o.win();
            x.lose();
        }
    }
    //test
    private boolean checkHorizontal() {
        String sum = "";
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                sum += data[i][j];
            }
            if (sum.equalsIgnoreCase("OOO") || sum.equalsIgnoreCase("XXX")) {
                return true;
            } else {
                sum = "";
            }
        }
        return false;
    }

    private boolean checkVertical() {
        String sum = "";
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                sum += data[j][i];
            }
            if (sum.equalsIgnoreCase("OOO") || sum.equalsIgnoreCase("XXX")) {
                return true;
            } else {
                sum = "";
            }
        }
        return false;
    }

    private boolean checkTile() {
        if (data[0][0] == 'O' && data[1][1] == 'O' && data[2][2] == 'O') {
            return true;
        } else if (data[0][0] == 'X' && data[1][1] == 'X' && data[2][2] == 'X') {
            return true;
        }
        return false;
    }

    private boolean checkTile2() {
        if (data[0][2] == 'O' && data[1][1] == 'O' && data[2][0] == 'O') {
            return true;
        } else if (data[0][2] == 'X' && data[1][1] == 'X' && data[2][0] == 'X') {
            return true;
        }
        return false;
    }

    private void addWinLose2() {
        win = currentPlayer;
        addWinLose();
    }

    public boolean checkWin() {
        if(checkVertical() || checkTile2() || checkHorizontal() || checkTile()){
            addWinLose2();
            return true;
        }
        switchTurn();
        return false;
    }

    public boolean checkDraw() {
        if (round == 9) {
            x.draw();
            o.draw();
            return true;
        }
        return false;
    }

    public void setRowCol(int row, int col) {
        data[row - 1][col - 1] = currentPlayer.getName();
        round++;
    }

    public void switchTurn() {
        if (currentPlayer == o) {
            currentPlayer = x;
        } else {
            currentPlayer = o;
        }
    }
}
