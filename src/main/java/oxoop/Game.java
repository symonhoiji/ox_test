package oxoop;


import java.util.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Symon
 */
public class Game {

    private Table table;
    private int row, col;
    private Player o, x;
    Scanner input = new Scanner(System.in);

    public Game() {
        o = new Player('O');
        x = new Player('X');
    }

    public void startGame() {
        table = new Table(o, x);
    }

    public void showWelcome() {
        System.out.println("Welcome to OX Game");
    }

    public void showTable() {
        char arr[][] = table.getData();
        System.out.println("  1 2 3");
        for (int row = 0; row < 3; row++) {
            System.out.print(row + 1);
            for (int col = 0; col < 3; col++) {
                System.out.print(" " + arr[row][col]);
            }
            System.out.println("");
        }
    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " turn");
    }

    public boolean inputRowCol() {
        System.out.print("Please input Row Col: ");
        table.setRowCol(input.nextInt(), input.nextInt());
        return true;
    }

    public boolean inputContinue() {
        System.out.print("Continue? (Y/N): ");
        if (input.next().equalsIgnoreCase("y")) {
            return true;
        }
        return false;
    }

    public void showBye() {
        System.out.println("Bye bye...");
    }

    public void getPlayerWinner() {
        Player p = table.getWinner();
        showWin(p);
    }

    public void showWin(Player player) {
        showTable();
        if (table.getWinner() == null) {
            System.out.println("Draw");
        } else {
            System.out.println("Player " + player.getName() + " Win");
        }
        showScoreboard();
    }
    public void showScoreboard(){
        System.out.println("O: Win: " + o.getWin() + " Lose: " + o.getLose() + " Draw: " + o.getDraw());
        System.out.println("X: Win: " + x.getWin() + " Lose: " + x.getLose() + " Draw: " + x.getDraw());
    }

    public void run() {
        showWelcome();
        do {
            startGame();
            runOnce();
        } while (inputContinue());
        showBye();

    }

    public void runOnce() {
        while (true) {
            showTable();
            showTurn();
            if (inputRowCol()) {
                if (table.checkWin()) {
                    getPlayerWinner();
                    return;
                } else if (table.checkDraw()) {
                    getPlayerWinner();
                    return;
                }
            }
        }

    }
}
