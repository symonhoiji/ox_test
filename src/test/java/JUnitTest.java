/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import oxoop.Player;
import oxoop.Table;

/**
 *
 * @author Symon
 */
public class JUnitTest {
    
    public JUnitTest() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }
    
    public void testWinCol1(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o,x);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.setRowCol(3, 1);
        assertEquals(true,table.checkWin());
    }
    public void testWinCol2(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o,x);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.setRowCol(3, 2);
        assertEquals(true,table.checkWin());
    }
    public void testWinCol3(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o,x);
        table.setRowCol(1, 3);
        table.setRowCol(2, 3);
        table.setRowCol(3, 3);
        assertEquals(true,table.checkWin());
    }
    public void testTile1(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o,x);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.setRowCol(3, 3);
        assertEquals(true,table.checkWin());
    }
    public void testTile2(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o,x);
        table.setRowCol(1, 3);
        table.setRowCol(2, 2);
        table.setRowCol(3, 1);
        assertEquals(true,table.checkWin());
    }
    public void testWinRow1(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o,x);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.setRowCol(1, 3);
        assertEquals(true,table.checkWin());
    }
    public void testWinRow2(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o,x);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.setRowCol(2, 3);
        assertEquals(true,table.checkWin());
    }
    public void testWinRow3(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o,x);
        table.setRowCol(3, 1);
        table.setRowCol(3, 2);
        table.setRowCol(3, 3);
        assertEquals(true,table.checkWin());
    }
    public void testSwitchPlayer(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o,x);
        table.switchTurn();
        assertEquals('X','O',table.getCurrentPlayer().getName());
    }
    public void testInsertWinPoint(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o,x);
        table.setRowCol(1, 3);
        table.setRowCol(2, 2);
        table.setRowCol(3, 1);
        table.checkWin();
        assertEquals(1,table.getCurrentPlayer().getWin()); 
    }
    public void testGetLosePoint(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o,x);
        table.setRowCol(1, 3);
        table.setRowCol(2, 2);
        table.setRowCol(3, 1);
        table.checkWin();
        assertEquals(0,table.getCurrentPlayer().getLose()); 
    }
    public void testFullGamev1(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o,x);
        table.setRowCol(1, 2);
        table.switchTurn();
        table.setRowCol(1, 1);
        table.switchTurn();
        table.setRowCol(2, 2);
        table.switchTurn();
        table.setRowCol(2, 1);
        table.switchTurn();
        table.setRowCol(3, 2);
        table.switchTurn();
        table.setRowCol(3, 3);
        assertEquals(true,table.checkWin());
    }
    public void testGetDrawPoint(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o,x);
        table.setRowCol(1, 1); //o
        table.switchTurn();
        table.setRowCol(2, 2); //x
        table.switchTurn();
        table.setRowCol(3, 1); //o
        table.switchTurn();
        table.setRowCol(2, 1); //x
        table.switchTurn();
        table.setRowCol(2, 3); //o
        table.switchTurn();
        table.setRowCol(1, 2); //x
        table.switchTurn();
        table.setRowCol(3, 2); //o
        table.switchTurn();
        table.setRowCol(3, 3); //x
        table.switchTurn();
        table.setRowCol(1, 3); //o
        table.checkWin();
        table.checkDraw();
        assertEquals(1,table.getCurrentPlayer().getDraw());
    }
// TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
